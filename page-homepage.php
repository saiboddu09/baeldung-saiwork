<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
      
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
  
    <section class="post-content">

      <?php the_content(); ?>

    </section> <!-- end article header -->
    
  </article> <!-- end article -->
  
  <?php endwhile; ?>
  
  <?php else : ?>
  
  <article id="post-not-found">
      <header>
        <h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
      </header>
      <section class="post-content">
        <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
      </section>
      <footer>
      </footer>
  </article>
  
  <?php endif; ?>

</div> <!-- end #main -->

<?php get_sidebar('sidebar2'); // sidebar 2 ?>

<?php get_footer(); ?>
