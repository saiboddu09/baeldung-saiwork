Simple changelog here
========================

2.8.6
-------------------------------------
- added original price to PPP adjusted buttons [BAEL-19054] (http://team.baeldung.com/browse/BAEL-19054)

2.8.5
-------------------------------------
- series page's icons have been fixed by reverting a new thrive css rule for image "captions" [BAEL-18934] (http://team.baeldung.com/browse/BAEL-18934)

2.8.4
-------------------------------------
- also stripping line breaking characters from anchor link headings to fix positioning bug of the actual anchor link [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.8.3
-------------------------------------
- anchor link br stripping fix [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.8.2
-------------------------------------
- anchor link script now strips out <br> tags from the headings, these were causing the anchor link to be misaligned [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.8.1
-------------------------------------
- moved anchor links to the right-hand side of headings [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)
- fixed issue with anchors appearing outside of article content [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.8.0
-------------------------------------
- added anchor links to headings using JS [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.7.9
-------------------------------------
- modified heading id generation to create a new element with the id to be used for positioning instead of the heading itself [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.7.8
-------------------------------------
- reverted changes to H2 & H3 margins [BAEL-17967] (http://team.baeldung.com/browse/BAEL-17967)

2.7.7
-------------------------------------
- moved vat/geo console messages to their own element with id geovat-info [BAEL-18232] (http://team.baeldung.com/browse/BAEL-18232)

2.7.6
-------------------------------------
- changed H2 and H3 top margin to padding for url # scrolling alignment [BAEL-17967] (http://team.baeldung.com/browse/BAEL-17967)

2.7.5
-------------------------------------
- added auto heading id generation for post content using heading text, hooking into the_content [BAEL-17957] (http://team.baeldung.com/browse/BAEL-17957)

2.7.4
-------------------------------------
- removed emoji support from frontend [BAEL-16856] (http://team.baeldung.com/browse/BAEL-16856)

2.7.3
-------------------------------------
- added editors and contributors to the customizations in the edit-post screen user list [BAEL-17457] (http://team.baeldung.com/browse/BAEL-17457)

2.7.2
-------------------------------------
- changed the grey background seen in the margins to white [BAEL-16790] (http://team.baeldung.com/browse/BAEL-16790)

2.7.1
-------------------------------------
- fixed dateModified mate value, which was displaying published date, and made both dates conform to the required ISO-8601 standard [BAEL-17404] (http://team.baeldung.com/browse/BAEL-17404)

2.7.0
-------------------------------------
- added new contributor role users to the author dropdown on the post edit screen [BAEL-17457] (http://team.baeldung.com/browse/BAEL-17457)

2.6.9
-------------------------------------
- added ability to add non-author roles to the list of authors that a post can have when editing and added custom author role to the list [BAEL-17457] (http://team.baeldung.com/browse/BAEL-17457)

2.6.8
-------------------------------------
- fixed broken auto ppp calculations in button prices that have different HTML for some thrive reason [BAEL-15912] (http://team.baeldung.com/browse/BAEL-15912)

2.6.7
-------------------------------------
- contact form 7 basic styling [BAEL-16786] (http://team.baeldung.com/browse/BAEL-16786)

2.6.6
-------------------------------------
- css added to make cookie notice appear above ezoic ads [BAEL-17197] (http://team.baeldung.com/browse/BAEL-17197)

2.6.5
-------------------------------------
- added fix for 100% viewport width ads that were causing the width of the page to be bigger than 100% [BAEL-16144] (http://team.baeldung.com/browse/BAEL-16144)

2.6.4
-------------------------------------
- added css class to hide new drip honeypot form elements [BAEL-16735] (http://team.baeldung.com/browse/BAEL-16735)

2.6.3
-------------------------------------
- remode filter that automatically adds paragraph in widget text content, which were breaking ad inserter placement [BAEL-16144] (http://team.baeldung.com/browse/BAEL-16144)

2.6.2
-------------------------------------
- changed further reading widget excerpts to divs instead of <p> for compatability with ad inserter plugin [BAEL-16144] (http://team.baeldung.com/browse/BAEL-16144)
- added non sticky widget option to freeze widgets in place [BAEL-16069] (http://team.baeldung.com/browse/BAEL-16069)

2.6.1
-------------------------------------
- stopped vat price from being effected by ppp by default [BAEL-15912] (http://team.baeldung.com/browse/BAEL-15912)

2.6.0
-------------------------------------
- widget custom js now removes float from ezoic ads that was causing ad overlap on scroll [BAEL-16082] (http://team.baeldung.com/browse/BAEL-16082)

2.5.9
-------------------------------------
- sticky widget CSS selector fixed [BAEL-15968] (http://team.baeldung.com/browse/BAEL-15968)

2.5.8
-------------------------------------
- added page-height widget display condition [BAEL-15968] (http://team.baeldung.com/browse/BAEL-15968)

2.5.7
-------------------------------------
- ppp auto price adjustment [BAEL-15912] (http://team.baeldung.com/browse/BAEL-15912)
- courses buy buttons can have the ppp coupons appended [BAEL-15912] (http://team.baeldung.com/browse/BAEL-15912)

2.5.6
-------------------------------------
- ie-only css fix for the home page course links [BAEL-14260] (http://team.baeldung.com/browse/BAEL-14260)

2.5.5
-------------------------------------
- switched country group numbers in ppp configuration [BAEL-14351] (http://team.baeldung.com/browse/BAEL-14351)

2.5.4
-------------------------------------
- removed large heading from ppp message used in location testing [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)
- fixed groupContent variable check [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)

2.5.3
-------------------------------------
- updated country groups [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)
- removed group message array, which is to now be used from within a thrive page [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)

2.5.2
-------------------------------------
- added countries to PPP message list [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)

2.5.1
-------------------------------------
- moved PPP message functionaility into assets to increase obfuscation [BAEL-12817] (http://team.baeldung.com/browse/BAEL-12817)

2.5.0
-------------------------------------
- fixed error relating to upgrade to php7.3 related to new regex library [BAEL-14087] (http://team.baeldung.com/browse/BAEL-14087)
- fixed count() function error possibly due to php7.3 upgrade [BAEL-14087] (http://team.baeldung.com/browse/BAEL-14087)

2.4.9
-------------------------------------
- added function to check if a given category is the parent category of the current post for use in Widget Logic [BAEL-14131] (http://team.baeldung.com/browse/BAEL-14131)

2.4.8
-------------------------------------
- fixed undefined constant warning in functions.php reltaing to further reading [BAEL-11382] (http://team.baeldung.com/browse/BAEL-11382)

2.4.7
-------------------------------------
- fixed Edit Post button visibility condition on post page [BAEL-11382] (http://team.baeldung.com/browse/BAEL-11382)
- fixed "expected string found array" error in the relative URL function [BAEL-11382] (http://team.baeldung.com/browse/BAEL-11382)

2.4.6
-------------------------------------
- fixed non-existent-index errors in the nav walker class [BAEL-11382] (http://team.baeldung.com/browse/BAEL-11382)

2.4.5
-------------------------------------
- fixed home page banner button alignment in IE10+ [BAEL-12106] (http://team.baeldung.com/browse/BAEL-12106)

2.4.4
-------------------------------------
- added ipdata to geo lookup services and made it first in the ordering [BAEL-12111] (http://team.baeldung.com/browse/BAEL-12111)

2.4.3
-------------------------------------
- switched order of geo IP providers [BAEL-12111] (http://team.baeldung.com/browse/BAEL-12111)
- added inclusion of geo-detected country code as a CSS class into the page body, for use in geo-targeted messages [BAEL-12111] (http://team.baeldung.com/browse/BAEL-12111)

2.4.2
-------------------------------------
- fixed intermittent sticky widget issue whne thrive leads loaded after widget calcs [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)

2.4.1
-------------------------------------
- changed sticky widget article height calculation to better position widgets witihn content [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)

2.4.0
-------------------------------------
- removed previous sticky widget functionailty [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)
- added new sticky widget frontend functionality [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)
- added custom widget field for sticky widget weight [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)
- new widget field added to all widgets as data-sticky-weight attribute [BAEL-11393] (http://team.baeldung.com/browse/BAEL-11393)

2.3.9
-------------------------------------
- changed course icon alignment to the top rather than centered [BAEL-11577] (http://team.baeldung.com/browse/BAEL-11577)

2.3.8
-------------------------------------
- fixed course icon aligment in the nav [BAEL-11577] (http://team.baeldung.com/browse/BAEL-11577)

2.3.7
-------------------------------------
- moved feathelight popup css asset [BAEL-11000] (http://team.baeldung.com/browse/BAEL-11000)
- changed grunt build order [BAEL-11000] (http://team.baeldung.com/browse/BAEL-11000)

2.3.6
-------------------------------------
- added grunt-contrib-copy to npm packages [BAEL-11000] (http://team.baeldung.com/browse/BAEL-11000)
- moved featherlight popup assets to dist folder with grunt [BAEL-11000] (http://team.baeldung.com/browse/BAEL-11000)
- adjusted enqueu urls for feather light assets [BAEL-11000] (http://team.baeldung.com/browse/BAEL-11000)

2.3.5
-------------------------------------
- added Featherlight JS popup library [BAEL-10239] (http://team.baeldung.com/browse/BAEL-10239)
- added custom styles for Featherlight popups with Thrive Leads content [BAEL-10239] (http://team.baeldung.com/browse/BAEL-10239
- adjusted pop responsive width [BAEL-10239] (http://team.baeldung.com/browse/BAEL-10239)
- set popup assets to only load on all pages except home page [BAEL-10239] (http://team.baeldung.com/browse/BAEL-10239)

2.3.4
-------------------------------------
- updated SVG injector lib and fixed code within it that was causing problems [BAEL-11001] (http://team.baeldung.com/browse/BAEL-11001)

2.3.3
-------------------------------------
- reverted nav colors back to #535353 [BAEL-10816] (http://team.baeldung.com/browse/BAEL-10816)

2.3.2
-------------------------------------
- reverted body text color and replaced every #535353 in style.css to #333 [BAEL-10816] (http://team.baeldung.com/browse/BAEL-10816)

2.3.1
-------------------------------------
- changed body text color [BAEL-10816] (http://team.baeldung.com/browse/BAEL-10816)

2.3.0
-------------------------------------
- fixed tag-page category display conditions [BAEL-10893] (http://team.baeldung.com/browse/BAEL-10893)

2.2.9
-------------------------------------
- fixed before/after content widgets display conditions [BAEL-10200] (http://team.baeldung.com/browse/BAEL-10200)

2.2.8
-------------------------------------
- set before/after content widgets to only display the first one with CSS [BAEL-10200] (http://team.baeldung.com/browse/BAEL-10200)

2.2.7
-------------------------------------
- removed debug code [BAEL-10828] (http://team.baeldung.com/browse/BAEL-10828)

2.2.6
-------------------------------------
- added grandparent category to inline JSON post meta [BAEL-10273] (http://team.baeldung.com/browse/BAEL-10273)

2.2.5
-------------------------------------
- moved position of post footer widget area to directly after post content [BAEL-10751] (http://team.baeldung.com/browse/BAEL-10751)

2.2.4
-------------------------------------
- fixed category count bug to include grandparent categories, on tag pages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300)

2.2.3
-------------------------------------
- fixed category count bug on tag pages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300)
- increased spacing between article links on tag/categorypages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300)

2.2.2
-------------------------------------
- added more spacing between category links on tag pages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 

2.2.1
-------------------------------------
- changed tag page title [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 
- fixed duplicate sort function bug [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 

2.2.0
-------------------------------------
- styled tag-page category links as parent categories [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 
- fixed display condition bug [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 

2.1.9
-------------------------------------
- added tag-count limit display condition to category links on tag page [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 

2.1.8
-------------------------------------
- added category links to tag pages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 
- created minimal link design on category and tag pages [BAEL-10300] (http://team.baeldung.com/browse/BAEL-10300) 

2.1.7
-------------------------------------
- Update: make URLs relative on category pages [BAEL-10315] (http://team.baeldung.com/browse/BAEL-10315)

2.1.6
-------------------------------------
- changed ipapi url to HTTPS to fix cross-content blocking [BAEL-10260] (http://team.baeldung.com/browse/BAEL-10260)

2.1.5
-------------------------------------
- Update: the root relative URLs were put back. [BAEL-9678] (http://team.baeldung.com/browse/BAEL-9678)

2.1.4
-------------------------------------
- updated geo lookup ipapi.com access token from new account [BAEL-10260] (http://team.baeldung.com/browse/BAEL-10260)

2.1.3
-------------------------------------
- added parent category tag to category pages with parents [BAEL-10240] (http://team.baeldung.com/browse/BAEL-10240)

2.1.2
-------------------------------------
- Fix: get rid of console errors caused by the VAT calc script [BAEL-10260] (http://team.baeldung.com/browse/BAEL-10260)

2.1.1
-------------------------------------
- added parent category to inline JSON post term meta [BAEL-10246] (http://team.baeldung.com/browse/BAEL-10246)

2.1.0
-------------------------------------
- Fix: remove not working API providers, added verbose console output [BAEL-BAEL-10141] (http://team.baeldung.com/browse/BAEL-10141)

2.0.9
-------------------------------------
- added widget areas before and after post content [BAEL-8993] (http://team.baeldung.com/browse/BAEL-8993)

2.0.8
-------------------------------------
- added custom pretty permalinks for tag filtering on category pages [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.7
-------------------------------------
- Update: the root relative URLs were removed in favor of the default Wordpress URLs. [BAEL-9678] (http://team.baeldung.com/browse/BAEL-9678)

2.0.6
-------------------------------------
- changed category page tag links to instead filter by tag [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)
- added tag filter to category title as breadcrumb [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.5
-------------------------------------
- increased Edit Category page count limit field maximum from 10 to 100 [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.4
-------------------------------------
- fix: show correct default tag count limit on category edit screen [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.3
-------------------------------------
- fix: show correct default category display value on tag edit screen [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.2
-------------------------------------
- made count limit a category meta field [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)
- added tag display control meta field on the edit tag screen for category pages [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.1
-------------------------------------
- added count-limit to tags displayed on category archive page [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

2.0.0
-------------------------------------
- added function to show all related post tags on category archive pages [BAEL-1551] (http://team.baeldung.com/browse/BAEL-1551)

1.9.9
-------------------------------------
- Fixed: Small bold font problem on a course page [BAEL-8500] (http://team.baeldung.com/browse/BAEL-8500)

1.9.8
-------------------------------------
- adjusted author page layout [BAEL-9151] (http://team.baeldung.com/browse/BAEL-9151)

1.9.7
-------------------------------------
- Hide articles index page from robots [BAEL-8951] (http://team.baeldung.com/browse/BAEL-8951)

1.9.6
-------------------------------------
- fully hid buysellads credits which was causing onconsistent widget margins [BAEL-9032] (http://team.baeldung.com/browse/BAEL-9032)

1.9.5
-------------------------------------
- Fix the Widget acting weird when the ribbon is visible [BAEL-8887] (http://team.baeldung.com/browse/BAEL-8887)

1.9.4
-------------------------------------
- fix for <pre> elements not wrapping [BAEL-8922] (http://team.baeldung.com/browse/BAEL-8922)

1.9.3
-------------------------------------
- Changed VAT price shortcode text [BAEL-8697] (http://team.baeldung.com/browse/BAEL-8697)

1.9.2
-------------------------------------
- moved author contact links to the side of their profile image

1.9.1
-------------------------------------
- Fix structured data on static pages [BAEL-7834] (http://team.baeldung.com/browse/BAEL-7834)

1.9.0
-------------------------------------
- fixed wwww {4 x w} in canonical links [BAEL-8223] (http://team.baeldung.com/browse/BAEL-8223)

1.8.9
-------------------------------------
- removed posts set to noindex from sitemaps [BAEL-8166] (http://team.baeldung.com/browse/BAEL-8166)

1.8.8
-------------------------------------
- Force canonical link URL to HTTPS [BAEL-7653] (http://team.baeldung.com/browse/BAEL-7653)

1.8.7
-------------------------------------
- Make canonical URLs absolute(with protocol and domain) [BAEL-7655] (http://team.baeldung.com/browse/BAEL-7655)

1.8.6
-------------------------------------
- Switch to free geoip APIs which support HTTPS [BAEL-7653] (http://team.baeldung.com/browse/BAEL-7653)

1.8.5
-------------------------------------
- Fix comment subscription option (broken email confirmation link URLs) [BAEL-7675] (http://team.baeldung.com/browse/BAEL-7675)

1.8.4
-------------------------------------
- Fixed Further Reading widget appearing on archive pages [BAEL-7441] (http://team.baeldung.com/browse/BAEL-7441)

1.8.3
-------------------------------------
- Fixed category/tag JSON meta on category/tag archive pages - they were showing the terms of the first post listed [BAEL-7441] (http://team.baeldung.com/browse/BAEL-7441)

1.8.2
-------------------------------------
- Fixed freegeoip API endpoint URL [BAEL-7431](http://team.baeldung.com/browse/BAEL-7431)
- Moved vat-rates.js and vat-calc.js to a separate directory [BAEL-7431](http://team.baeldung.com/browse/BAEL-7431)

1.8.1
-------------------------------------
- Removed term slug and term id from embedded meta JSON (http://team.baeldung.com/browse/BAEL-7441)

1.8.0
-------------------------------------
- Added functions to embed terms & further reading data as JSON into the footer [BAEL-7441] (http://team.baeldung.com/browse/BAEL-7441)

1.7.9
-------------------------------------
- Added stacked footer widgets (menu) [BAEL-6781](http://team.baeldung.com/browse/BAEL-6781)

1.7.8
-------------------------------------
- Fixed Structured Data: force absolute image URLs in meta [BAEL-7427](http://team.baeldung.com/browse/BAEL-7427)

1.7.7
-------------------------------------
- Remove the "built with" message from the Wordpress Admin [BAEL-3428](http://team.baeldung.com/browse/BAEL-3428)

1.7.6
-------------------------------------
- Fixed broken RSS feed [BAEL-7389](http://team.baeldung.com/browse/BAEL-7389)

1.7.5
-------------------------------------
- Fixed broken XML sitemaps [BAEL-7391](http://team.baeldung.com/browse/BAEL-7391)

1.7.4
-------------------------------------
- Fixed sidebar widget jump at page load [BAEL-5633](http://team.baeldung.com/browse/BAEL-5633)
- Fixed IE10/11 columns layout issue on screens with sizes >= 920px ... < 1200px [BAEL-5633](http://team.baeldung.com/browse/BAEL-5633)
- Deprecated bower version fix [BAEL-5633](http://team.baeldung.com/browse/BAEL-5633)

1.7.3
-------------------------------------
- Fix author's avatar images [BAEL-7245](http://team.baeldung.com/browse/BAEL-7245)

1.7.2
-------------------------------------
- Make Wordpress URLs relative [BAEL-4334](http://team.baeldung.com/browse/BAEL-4334)
- Fix "Read More" links on Author archive page [BAEL-7065](http://team.baeldung.com/browse/BAEL-7065)

1.7.1
-------------------------------------
- Center the artile content on tablet [BAEL-6853](http://team.baeldung.com/browse/BAEL-6853)

1.7.0
-------------------------------------
- Make only h2 and h3 headings bold(700) by default [BAEL-6467](http://team.baeldung.com/browse/BAEL-6467)

1.6.9
-------------------------------------
- Make global h1/h2/h3/h4/h5/h6 headings bold(700) by default [BAEL-6238](http://team.baeldung.com/browse/BAEL-6238)
- Fixed further reading menu styling issues in the New Menu [BAEL-5951](http://team.baeldung.com/browse/BAEL-5951)
- Fixed further reading menu styling issues for less than 3 posts [BAEL-5951](http://team.baeldung.com/browse/BAEL-5951)

1.6.8
-------------------------------------
- Make descriptions visible on new category pages [BAEL-6512](http://team.baeldung.com/browse/BAEL-6512)

1.6.7
-------------------------------------
- Make iframe video responsive [BAEL-5957](http://team.baeldung.com/browse/BAEL-5957)

1.6.6
-------------------------------------
- Re-design page template layout to use flexbox [BAEL-5820](http://team.baeldung.com/browse/BAEL-5820)
- Huge CSS refactor [BAEL-5820](http://team.baeldung.com/browse/BAEL-5820)
- Removed Bootstrap3(except collapse.js) [BAEL-5820](http://team.baeldung.com/browse/BAEL-5820)

1.6.5
-------------------------------------
- Archive category page [BAEL-4297](http://team.baeldung.com/browse/BAEL-4297)
- Added new feature: custom category archive icons [BAEL-4297](http://team.baeldung.com/browse/BAEL-4297)

1.6.4
-------------------------------------
- Fixed dropdown menu align [BAEL-5936](http://team.baeldung.com/browse/BAEL-5936)
- Fixed dropdown menu svg icon scale bug in IE11 [BAEL-5936](http://team.baeldung.com/browse/BAEL-5936)
- Fixed the long-text wrapping in titles/paragraphs [BAEL-5379](http://team.baeldung.com/browse/BAEL-5379)
- Remove !important flags from theme styles. Minor refactor. [BAEL-5645](http://team.baeldung.com/browse/BAEL-5645)

1.6.3
-------------------------------------
- Add new template: "Homepage With No Sidebar" [BAEL-4516](http://team.baeldung.com/browse/BAEL-4516)

1.6.2
-------------------------------------
- Clean up unused css from the theme's ./style.css [BAEL-5644](http://team.baeldung.com/browse/BAEL-5644)

1.6.1
-------------------------------------
- Added post tag display option [BAEL-5621](http://team.baeldung.com/browse/BAEL-5621)
- Removed automatic .lead class on a first paragraph in a content.

1.6.0
-------------------------------------
- Added the Title Tag theme support to get rid of the old wp_title format. [BAEL-5634](http://team.baeldung.com/browse/BAEL-5634)

1.5.9
-------------------------------------
- Changed 200 weight font declarations to 300 [BAEL-4526](http://team.baeldung.com/browse/BAEL-4526)
- Removed raleway @import statement from main CSS [BAEL-4526](http://team.baeldung.com/browse/BAEL-4526)

1.5.8
-------------------------------------
- Added feature to set the color on a sponsored tag [BAEL-5396](http://team.baeldung.com/browse/BAEL-5396)

1.5.7
-------------------------------------
- Cleaned up the media query declarations [BAEL-5128](http://team.baeldung.com/browse/BAEL-5128)
- Removed some unused "old theme" styles [BAEL-5128](http://team.baeldung.com/browse/BAEL-5128)
- Fix the media query bug on 943px(refactor mobile menu styles) [BAEL-5128](http://team.baeldung.com/browse/BAEL-5128)

1.5.6
-------------------------------------
- Replaced footer logo image [BAEL-4744](http://team.baeldung.com/browse/BAEL-4744)
- Optimized theme images [BAEL-4744](http://team.baeldung.com/browse/BAEL-4744)
- Removed unused images [BAEL-4744](http://team.baeldung.com/browse/BAEL-4744)
- Optimize favicons (.png images in ./favicon folder) [BAEL-4744](http://team.baeldung.com/browse/BAEL-4744)
- Fix sidebar widget alignment [BAEL-5252](http://team.baeldung.com/browse/BAEL-5252)

1.5.5
-------------------------------------
- H2 and H3 are now bold by default in all posts [BAEL-5242](http://team.baeldung.com/browse/BAEL-5242)

1.5.4
-------------------------------------
- Fixed Safari flexbox issue and mobile menu, added drop-down menu paddings [BAEL-5245](http://team.baeldung.com/browse/BAEL-5245)
- Adjusted mobile icon max-width [BAEL-5128](http://team.baeldung.com/browse/BAEL-5128)
- Aligned menu items to center

1.5.3
-------------------------------------
- AquaSVG/Thrive Leads conflict and state-switch bug fix [BAEL-3751](http://team.baeldung.com/browse/BAEL-3751)

1.5.2
-------------------------------------
- Internet explorer menu bug fix [BAEL-4620](http://team.baeldung.com/browse/BAEL-4620)


1.5.1
-------------------------------------
- Added custom hook - ba_pre_main_content - to header. For use by Full Page menu plugin [BAEL-3619](http://team.baeldung.com/browse/BAEL-3619)


1.5.0
-------------------------------------
- Sponsored post Java Weekly element [BAEL-5031](http://team.baeldung.com/browse/BAEL-5031)


1.4.9
-------------------------------------
- Footer configuration moved to one widget [BAEL-4872](http://team.baeldung.com/browse/BAEL-4872)
- Added whitespace between the menu and the sidebar widget on the homepage [BAEL-4878](http://team.baeldung.com/browse/BAEL-4878)

1.4.8
-------------------------------------
- Internet Explorer Start Here Page fix [BAEL-4728](http://team.baeldung.com/browse/BAEL-4728)

1.4.7
-------------------------------------
- Deleted redundant code from the list elements in the menu [BAEL-4327](http://team.baeldung.com/browse/BAEL-4327)
- Fixed Thrive opt-in box width issue [BAEL-4331](http://team.baeldung.com/browse/BAEL-4331)

1.4.6
-------------------------------------
- Fixed Thrive widgets width and position issue [BAEL-3946](http://team.baeldung.com/browse/BAEL-3946)
- Fixed Thrive footer opt-ins image container size issue [BAEL-3946](http://team.baeldung.com/browse/BAEL-3946)
- Reduced Page/Post width [BAEL-3946](http://team.baeldung.com/browse/BAEL-3946)
- Article series functionality removed [BAEL-3946](http://team.baeldung.com/browse/BAEL-3946)
- Added final version of SVG Aqua Sprite support [BAEL-3416](http://team.baeldung.com/browse/BAEL-3416)

1.4.5
-------------------------------------
- Fixed the main menu positon issue [BAEL-3750](http://team.baeldung.com/browse/BAEL-3750)

1.4.4
-------------------------------------
- Responsive menu edits [BAEL-3750](http://team.baeldung.com/browse/BAEL-3750)
- Main/footer container edits [BAEL-3750](http://team.baeldung.com/browse/BAEL-3750)
- SVG support fix [BAEL-3416](http://team.baeldung.com/browse/BAEL-3416)

1.4.31
-------------------------------------
- VAT support minor fix [BAEL-3533](http://team.baeldung.com/browse/BAEL-3533)

1.4.3
-------------------------------------
- Added article series functionality [BAEL-2324](http://team.baeldung.com/browse/BAEL-2324)
- Added VAT support [BAEL-2599](http://team.baeldung.com/browse/BAEL-2599)
- First version of SVG support [BAEL-3416](http://team.baeldung.com/browse/BAEL-3416)

1.4.2
-------------------------------------
- Fixed menu hover issue [BAEL-2884](http://team.baeldung.com/browse/BAEL-2884)

1.4.1
-------------------------------------
- Added HTML support in menu items description [BAEL-2836](http://team.baeldung.com/browse/BAEL-2836)

1.4.0
-------------------------------------
- Added new feature to hide comments completely from a post or page [BAEL-2634](http://team.baeldung.com/browse/BAEL-2634)

1.3.11
-------------------------------------
- Fixed visibility issue on a single post page

1.3.10
-------------------------------------
- Initial version
