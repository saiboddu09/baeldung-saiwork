          </div> <!-- end .flex-wrap -->

        </div> <!-- end .container-inner -->

    </div> <!-- end #content.container -->

      <footer id="footer" class="content-info" role="contentinfo">

        <div class="container">

          <div class="container-inner">

            <div class="column-wrapper row">

              <div class="logo-desktop">
                <img src="<?php echo get_template_directory_uri(); ?>/icon/logo.svg" alt="The Baeldung logo">
              </div>

              <div class="widgets">
                <?php dynamic_sidebar( 'footer_columns' ); ?>
              </div>

              <div class="stacked-rows">
                <?php dynamic_sidebar( 'footer_stacked_rows' ); ?>
              </div>

              <div class="logo-mobile">
                <img src="<?php bloginfo('template_directory');?>/icon/whiteleaf.svg" alt="The Baeldung Logo">
              </div>

            </div> <!-- end .column-wrapper -->

          </div> <!-- end container-inner -->

        </div> <!-- end .container -->

      </footer> <!-- end footer -->

    </div> <!-- end #wrap -->

    <?php wp_footer(); // js scripts are inserted using this function ?>

  </body>

</html>
