<?php
/**
 * Class Name: Yamm_Nav_Walker & Yamm_Fw_Nav_Walker
 * GitHub URI: https://github.com/macdonaldr93/yamm-nav-walker
 * Description: A custom WordPress nav walker class to implement the Yamm 3 bootstrap mega menu navigation style in a custom theme using the WordPress built in menu manager.
 * Version: 1.0.0
 * Author: Ryan Macdonald - @macdonaldr93
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

class Yamm_Nav_Walker extends Walker_Nav_Menu
{
  static public $reg_icons = array();

  function addIcon($menu_id, $icon){
    $icon_mod = in_array($icon, self::$reg_icons) ? uniqid($icon . '_') : $icon;

    if(!empty($menu_id)) {
      self::$reg_icons[$menu_id] = $icon_mod;

      return $icon_mod;
    }
  }

  function getRawIcon($id){
    $icon_id = get_post_meta( $id, '_menu_item_icon', true );
    $icon_path = get_attached_file( $icon_id );
    $icon = array();
    preg_match("/(.*\/)([\w\s_-]*)\.svg$/", $icon_path, $icon);
    return $icon;
  }

  function getIconHTML($item){

	if(!$item->svgid){
		$icon = self::getRawIcon($item->ID);
		$icon_html = isset($icon[0]) && file_exists($icon[0]) ? file_get_contents($icon[0]) : '';

	}else{
		$icon_html = '<svg data-src="/wp-content/uploads/aqua-svg-sprite/aqua-svg-general-sprite.svg#'.$item->svgid.'"></svg>';
	}
	
	$icon_ID = isset(self::$reg_icons[$item->ID]) ? self::$reg_icons[$item->ID] : 0;
	if (!empty($icon_ID) && empty(preg_grep("/<svg[^>]*?id=/", explode("\n", $icon_html)))) {
	  $icon_html = str_replace('<svg', '<svg id="IconDefaultID"', $icon_html);
	}
	$icon_html = !empty($icon_ID) ? preg_replace("/(<svg[^>]*?id=\").*?(\"[^>]*?>)/", '$1' . $icon_ID . '$2', $icon_html) : $icon_html;

    return $icon_html;

  }


  function start_lvl(&$output, $depth = 0, $args = array())
  {
    $output .= ($depth == 0) ? "\n<div class=\"nav--dropdown\">\n" . "\n<ul class=\"nav--dropdown_content\">\n"  : "\n<ul class=\"nav--dropdown_content\">\n";
  }

  function end_lvl(&$output, $depth=0, $args=array()) {
	$output .= "</ul></div>";
  }

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    $item_html = '';
    parent::start_el($item_html, $item, $depth, $args);

    if ($item->is_dropdown) {
      $opener = '<a id="menu-item-' . esc_attr( $item->ID ) . '" class="nav--menu_item_anchor nav--menu_item_withDropdown" href="javascript:void(0)">';
      $closer = '&nbsp;<span class="menu--item_normal">▼</span><span class="menu--item_active">▲</span></a>';
      $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', $opener . '$1' . $closer, $item_html);

    } elseif ($item->is_dropdown_item) {
      $icon_html = self::getIconHTML($item);
      $item_html  = '
        <li class="nav--dropdown_item">
          <a class="dropdown--item_anchor transition--background" href="' . esc_url($item->url) . '" title="' . esc_attr($item->attr_title) . '">
            <div class="dropdown--item_icon">
              ' . $icon_html . '
            </div>

            <div class="dropdown--item_text">
              <h3 class="dropdown--item_title">
                ' . $item->title . '
              </h3>
              <p class="dropdown--item_excerpt">
                ' . $item->description . '
              </p>
            </div>
          </a>
        </li>
      ';

    } else {
      $opener = '<a id="menu-item-' . esc_attr( $item->ID ) . '" class="nav--menu_item_anchor" href="' . esc_url($item->url) . '">';
      $closer = '</a>';
      $icon_html = self::getIconHTML($item);
      $item_html = !empty($icon_html) ? preg_replace('/<a[^>]*>(.*)<\/a>/iU', $opener . $icon_html . $closer, $item_html) : str_replace('<a', '<a class="nav--menu_item_anchor"', $item_html);
    }

    $item_html = apply_filters('roots_wp_nav_menu_item', $item_html);
    $output .= $item_html;
  }

  function end_el(&$output, $item, $depth=0, $args=array()) {
    $output .= "";
  }

  function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
    //setup
    $childs = isset($children_elements[$element->ID]) ? $children_elements[$element->ID] : '';
    $element->is_dropdown_item = ($element->menu_item_parent !== 0) ? $element->menu_item_parent : 0;
    $element->is_dropdown = (!empty($childs) && (($depth + 1) < $max_depth || ($max_depth === 0)));

    // classes
    if ($depth === 0) {
      $highlight = !empty( $element->highlight ) ? 'nav--menu_item_darkened' : '';
      $element->classes[] = 'nav--menu_item ' . $highlight;
      if( !empty( $childs ) ) {
        $element->classes[] = 'nav--menu_has_' . count($childs) . '_children';
      }
    }

    if ($element->is_dropdown_item) {
      $element->classes[] = 'nav--dropdown_item';
    }

    $start = 'autostart';
    $handler = '';

    if ($element->is_dropdown && $depth === 0) { // contain childs with icons

      $child_func = array();
      $init = array();
      $start = 'manual';

      foreach ($childs as $ch) {
        $icon = self::getRawIcon($ch->ID);

		if(!empty($icon)){
			$icon = $icon[2];
		}else{
			$icon = $ch->svgid;
		}
		
        $func = !empty($icon) ? implode('', array_map("ucfirst", explode('-', $icon))) : '';

        if(!empty($func)) {
          $unique_func = self::addIcon($ch->ID, $func);
          $initjs[] = $unique_func . ' = new Vivus(\'' . $unique_func . '\', {type: \'delayed\', duration: 60,  start: \'' . $start . '\'});';
          $child_func[] = $unique_func;
        }
      }

      if (!empty($child_func)) {
        $handler = '
          document.getElementById(\'menu-item-' . $element->ID . '\').onmouseenter = function(){
            ' . implode(".reset().play();\n", $child_func) . '.reset().play();
          };
        ';

        wp_add_inline_script( 'vivus', '

          function svginit_' . $element->ID . '() {
            ' . implode("\n", $initjs) . '
            ' . $handler . '
          }
          document.addEventListener("ready-to-animate-svg", svginit_' . $element->ID . ');
        ');
      }


    } elseif( (!empty($element->icon) || !empty($element->svgid)) && $depth === 0) { // has own icon

      $icon = self::getRawIcon($element->ID);

	  if(!empty($icon)){
		$icon = $icon[2];
	  }else{
		$icon = $element->svgid;
	  }

      $func = !empty($icon) ? implode('', array_map("ucfirst", explode('-', $icon))) : '';

      if (!empty($func)) {
        $unique_func = self::addIcon($element->ID, $func);

        $handler = '
          document.getElementById(\'menu-item-' . $element->ID . '\').onmouseenter = function(){
            ' . $unique_func . '.reset().play();
          };
        ';

        wp_add_inline_script( 'vivus', '
          function svginit_' . $unique_func . '() {
            '. $unique_func . ' = new Vivus(\'' . $unique_func . '\', {type: \'delayed\', duration: 60,  start: \'' . $start . '\'});
            ' . $handler . '
          }
          document.addEventListener("ready-to-animate-svg", svginit_' . $unique_func . ');
        ');

      }
    }

    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }

}

/**
 * Remove the id="" on nav menu items
 * Return 'menu-slug' for nav menu classes
 */
function yamm_roots_nav_menu_css_class($classes, $item) {
  $slug    = sanitize_title($item->title);
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
  $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

  $classes[] = 'menu-' . $slug;

  $classes = array_unique($classes);

  return array_filter($classes);
}
add_filter('nav_menu_css_class', 'yamm_roots_nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', '__return_null');

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Use Yamm_Nav_Walker() by default
 */
function yamm_roots_nav_menu_args($args = '') {
  $yamm_roots_nav_menu_args['container'] = false;

  if (!$args['items_wrap'])
  {
    $yamm_roots_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
  }

  if (!$args['depth'])
  {
    $yamm_roots_nav_menu_args['depth'] = 2;
  }

  return array_merge($args, $yamm_roots_nav_menu_args);
}
add_filter('wp_nav_menu_args', 'yamm_roots_nav_menu_args');

?>
