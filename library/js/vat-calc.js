(function($){
  /*** Add prices with vat based on eu country to thrive checkout buttons ***/

  //get vat rates using https://github.com/staaky/vatrates
  var vatRates = new VATRates();

  var geoipProviders = {
    '0': {
      'api_name': 'ipdata',
      'api_data_field': 'country_code',
      'api_url': 'https://api.ipdata.co/',
      'api_token': '?api-key=5b4f43314209a9f4416c0b44289c12c42ff15945d46e884eae2574d8',
    },
    '1': {
      'api_name': 'ipapi.com',
      'api_data_field': 'country_code',
      'api_url': 'https://api.ipapi.com/api/',
      'api_token': '?access_key=268683fd1bba23cd60da8245820be738',
    },
    '2': {
      'api_name': 'ipinfo.io',
      'api_data_field': 'country',
      'api_url': 'https://ipinfo.io/',
      'api_token': '?token=c9d07ba51ab69e',
    },
  };


  //truncate floats
  function toTwoDP(num, fixed) {
    var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
  }


  function getQueryURL(data, ip) {

    if (!data) { return false; }

    var URL = '';

    URL += data.api_url;

    URL += ip;

    if ( data.api_token ) {
      URL += data.api_token;
    }

    return URL;
  }

  function getCountryCode(data, field) {
    var code = data;
    var field_arr = field.split(".");

    for (var i = 0; i < field_arr.length; i++) {
      code = code[field_arr[i]];
    }

    return code;
  }

  function getPPPGroupCodes(){
	  return {
		"TW" : 2,
		"TH" : 2,
		"CO" : 2,
		"BR" : 2,
		"PL" : 2,
		"LV" : 2,
		
		"CN" : 1,
		"IN" : 1,
		"VN" : 1,
		"AR" : 1,
		"PH" : 1,
		"MY" : 1,
		"LK" : 1,
		"PK" : 1,
		"MX" : 1,
		"RU" : 1,
		"RS" : 1,
		"UA" : 1,
		"RO" : 1,
		"TR" : 1,
	};
  }
  
function filter_ppp_price(price, code){
	//look for ppp js configuration
	var groupCodes = getPPPGroupCodes();
	if (typeof groupContent !== 'undefined') {
		//if the country is in a pp country
		if( groupCodes[code] ){
			//get the discount amount and modifier (for fixed subtractions and percentages)
			ppp_discount = groupContent[groupNum]['ppp_amount'];
			ppp_op = groupContent[groupNum]['ppp_operator'];
			
			//adjust the priced based on the modifier
			if(ppp_op=='-'){
				price = price - ppp_discount;
			}else if (ppp_op=='%'){
				price = price * ((100-ppp_discount) / 100);
			}
			
		}
		
		//not a ppp group country
	
	}
	return price;
}

function addCouponToButtonURL($sel, code){
	var groupCodes = getPPPGroupCodes();
	if (typeof groupContent !== 'undefined') {
		//if the country is in a pp country
		if( groupCodes[code] ){
			//for each button with selector $sel
			$($sel + ' .tcb-button-link').each(function(){
				var url = $(this).attr('href');
				url = url + '&coupon_code=' + groupContent[groupNum]['ppp_code'];
				$(this).attr('href', url);
			});
		}
	}
}

function adjustPPPbuttonPrice($sel, code){
	
	//for each button with the selector $sel
	$($sel).each(function(){
		//thrive elements can contain many nested elements used for styling, this code hunts to the deepest child (the actual text) and changes it
		//select the children of the button
		var $buttonContents = $(this).find('.tcb-button-text').children();
		//find the deepest child
		while( $buttonContents.length ) {
		  $buttonContents = $buttonContents.children();
		}
		//get the actual button text
		$buttonText = $buttonContents.end().html();
		//get the price from the text
		var original_price = $buttonText.match(/[$][\d]*/);
		//remove $ from price
		original_price = original_price[0].replace('$','');
		//adjust price for PPP
		var price = filter_ppp_price(original_price, code);
		//handle decimal places
		price = Number(price).toFixed(2);
		//replace old price with new price in button text
		$buttonText = $buttonText.replace(original_price, price);
		
		//html for top line of button containing original price
		var old_price_line = "<div style='padding-bottom: 5px;'>Without PPP: <span style='text-decoration:line-through;'>$"+original_price+"</span></div>";
		//html for bottom line of button containg PPP price
		var new_price_line = "<div>"+$buttonText+"</div>";
		
		//do the actual text replacement
		$buttonContents.end().html(old_price_line + new_price_line);
	});
	
}

  function processQuery(code) {

	//add country code as class to body
  	$('body').addClass('country-' + code);
	
	//PPP country code processing
	
	var groupCodes = getPPPGroupCodes();
	/*
	code to be used within a Thrive page
var groupContent = {
	1 : {
			'message' : 'I support Parity Purchasing Power on my beginner courses, as I want to make sure they\'re affordable for every developer around the world. Use the code "PARITY123" to get 30% off the listed prices if you need it.'
		},
	2 : {
			'message' : 'I support Parity Purchasing Power on my beginner courses, as I want to make sure they\'re affordable for every developer around the world. Use the code "PARITY456" to get 20% off the listed prices if you need it.'
		},
}
	*/
	
	

	
	//look for ppp js configuration
	if (typeof groupContent !== 'undefined') {
		//if the country is in a pp country
		if( groupCodes[code] ){
			//get the group num
			groupNum = groupCodes[code];
			//build the html for the ppp message
			var blockContent = '<div class="ppp-message"><p>'+groupContent[groupNum]['message']+'</p></div>';
			//fill the ppp message placeholder with the html
			$('.ppp-message-container').html(blockContent);
			//replace the button prices with the pp adjusted prices
			adjustPPPbuttonPrice('.thrv-button.pppable', code);
			addCouponToButtonURL('.thrv-button.pppable', code);
		}
	}else{
		console.log('PPP message content not set');
	}

	
    //check country code is valid
    if (vatRates.isVATCountry(code)) {

      //get decimalized vat mark up multiplier
      var vatMultiplier = ( vatRates.getStandardRate(code) / 100 ) + 1;

      //get all placeholders on the page
      $('.price-with-vat').each(function(){

        //get the price
        var price = $(this).attr('data-price');
		if($(this).hasClass('pppable') ){
			price = filter_ppp_price(price, code);
		}
		
        if(!isNaN(price)){
          //apply VAT without rounding, truncate to 2dp
          var withvat = price * vatMultiplier;
          withvat = parseFloat(toTwoDP(withvat, 2));
          withvat = withvat.toFixed(2);

          //add text content to placeholder
          $(this).html('($' + withvat + ' with VAT)');

        }else{
          $errorContainer.append('VAT Calc Error: ' + price + ' is not a number');
        }

      });

    } else{
      //not an eu country
    }

  }

  function processProvider(providerData, data) {

    var field = providerData.api_data_field;

    if (field) {
      var code = getCountryCode(data, field);

      processQuery(code);
    }
  }

  $(document).ready(function(){

	//create error container for automated tests
	$('body').append('<div id="geovat-info" style="display:none;"></div>');
	$errorContainer = $('#geovat-info');
	
    $.getJSON('https://api.ipify.org/?format=json').success( function(IPdata) {

      var IP = IPdata.ip;

      if (IP) {

        var currentState = 0,
            successFunc = function(i) {
              $errorContainer.append('VAT Calc Notice: geoIP API provider is set to ' + geoipProviders[i].api_name);
            },
            failFunc = function(i) {
              $errorContainer.append('VAT Calc Error: failed to load geoIP API from ' + geoipProviders[i].api_name);
            },
            epicFailFunc = function(i) {
              $errorContainer.append('VAT Calc Error: there are no working geoIP API providers left - you have reached the API usage limit or the API connection details were changed');
            },
            switchProvider = function(i) {

              if (i < Object.keys(geoipProviders).length) {

                var providerData = geoipProviders[i];
                var queryURL = getQueryURL(providerData, IP);

                $.getJSON(queryURL).success( function (data, status) {

                  if (data && data.success !== false) {

                    successFunc(i);
                    processProvider(providerData, data);

                  } else {

                    failFunc(i);
                    currentState++;
                    switchProvider(currentState);

                  }

                }).fail(function(){

                  failFunc(i);
                  currentState++;
                  switchProvider(currentState);

                });

              } else {

                epicFailFunc();

              }

            };

        switchProvider(currentState);

      } else {

        $errorContainer.append('IP data API Error: no IP provided');

      }

    }).fail(function(){

      $errorContainer.append('IP data API Error: IP data provider failed to respond');

    });

  });

})(jQuery);
