<?php get_header(); ?>

  <article id="post-not-found" class="clearfix">

    <header class="page-header">
      <h1 class="single-title"><?php _e("Epic 404","wpbootstrap"); ?> <br><?php _e("Article Not Found","wpbootstrap"); ?></h1>
      <p><?php _e("This is embarassing. We can't find what you were looking for.","wpbootstrap"); ?></p>
    </header> <!-- end article header -->

    <section class="post-content">

      <br>
      <p><?php _e("Whatever you were looking for was not found, but maybe try looking again or search using the form below.","wpbootstrap"); ?></p>

      <?php get_search_form(); ?>

    </section> <!-- end article section -->

  </article> <!-- end article -->

</div> <!-- end #main -->

<?php get_footer(); ?>
